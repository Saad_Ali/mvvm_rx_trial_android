package com.example.mvvm_rx_trial_android.di.module;


import com.example.mvvm_rx_trial_android.Products.model.remote.ProductService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiModule {
    @Provides
    @Singleton
    public ProductService providesProductsService(Retrofit retrofit) {
        return retrofit.create(ProductService.class);
    }

}
