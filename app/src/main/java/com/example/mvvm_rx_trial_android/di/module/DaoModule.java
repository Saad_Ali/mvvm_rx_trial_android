package com.example.mvvm_rx_trial_android.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.mvvm_rx_trial_android.AppDatabase;
import com.example.mvvm_rx_trial_android.Products.model.local.ProductDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class DaoModule {

    @Provides
    @Singleton
    public AppDatabase provideAppDatabase(Application app) {
        return Room.databaseBuilder(app,
                AppDatabase.class, "products_localDB.db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    public ProductDao provideProductDao(AppDatabase appDatabase) {
        return appDatabase.productDao();
    }


}
