package com.example.mvvm_rx_trial_android.di;


import com.example.mvvm_rx_trial_android.di.module.ApiModule;
import com.example.mvvm_rx_trial_android.di.module.AppModule;
import com.example.mvvm_rx_trial_android.di.module.DaoModule;
import com.example.mvvm_rx_trial_android.di.module.NetModule;
import com.example.mvvm_rx_trial_android.Products.view.MainActivity;
import com.example.mvvm_rx_trial_android.Products.viewmodel.ProductsViewModel;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(
        modules = {
                AppModule.class,
                NetModule.class,
                DaoModule.class,
                ApiModule.class
        }
)
public interface AppComponent {

    void inject(ProductsViewModel productsViewModel);

    void inject(MainActivity mainActivity);


}
