package com.example.mvvm_rx_trial_android.Products.model.local;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.mvvm_rx_trial_android.Products.model.BasicModels.ProductModel;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM Product ")
    LiveData<List<ProductModel>> getProductList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProductList(List<ProductModel> ProductList);
}
