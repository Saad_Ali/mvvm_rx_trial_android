package com.example.mvvm_rx_trial_android.Products.model.BasicModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsListModel {

    @SerializedName("products")
    private List<ProductModel> ProductsList;

    public List<ProductModel> getProductsList() {
        return ProductsList;
    }

    public void setProductsList(List<ProductModel> courseList) {
        this.ProductsList = courseList;
    }
}
