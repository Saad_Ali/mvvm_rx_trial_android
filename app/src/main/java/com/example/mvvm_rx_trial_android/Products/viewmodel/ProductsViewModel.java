package com.example.mvvm_rx_trial_android.Products.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.mvvm_rx_trial_android.App;
import com.example.mvvm_rx_trial_android.Products.model.BasicModels.ProductModel;
import com.example.mvvm_rx_trial_android.Products.model.repo.ProductsRepository;
import com.example.mvvm_rx_trial_android.helpers.livedata.AbsentLiveData;
import com.example.mvvm_rx_trial_android.helpers.livedata.Resource;

import java.util.List;

import javax.inject.Inject;

public class ProductsViewModel extends ViewModel {

    public final MutableLiveData<List<ProductModel>> productsList;
    @Inject
    ProductsRepository productsRepository;
    private Observer<Resource<List<ProductModel>>> productsListObserver;
    private LiveData<Resource<List<ProductModel>>> productsListLiveData;

    public ProductsViewModel() {
        App.getAppComponent().inject(this);


        productsList = new MutableLiveData<>();

        productsListLiveData = AbsentLiveData.create();
        productsListObserver = getProductsListObserver();

    }

    private Observer<Resource<List<ProductModel>>> getProductsListObserver() {
        return productsListResource -> {
            if (productsListResource != null) {
                switch (productsListResource.getStatus()) {
                    case LOADING:
                        Log.i(ProductsViewModel.class.getName(),"loading ......");
                        break;
                    case SUCCESS:
                        if (productsListResource.getData() != null) {
                            productsList.setValue(productsListResource.getData());

                        }

                        break;
                    case ERROR:
                        Log.i(ProductsViewModel.class.getName(),"Error");

                }
            }
        };
    }

    public void fetchProductsList() {
        productsListLiveData = productsRepository.getProductsList();
        productsListLiveData.observeForever(productsListObserver);
    }

    public MutableLiveData<List<ProductModel>> getProductsList() {
        return productsList;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (productsListLiveData != null)
            productsListLiveData.removeObserver(productsListObserver);
    }
}
