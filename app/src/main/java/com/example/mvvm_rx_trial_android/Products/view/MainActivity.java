package com.example.mvvm_rx_trial_android.Products.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.mvvm_rx_trial_android.Products.model.BasicModels.ProductModel;
import com.example.mvvm_rx_trial_android.Products.viewmodel.ProductsViewModel;
import com.example.mvvm_rx_trial_android.R;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ProductsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(ProductsViewModel.class);

        listenToProductsList();

        viewModel.fetchProductsList();

    }

    private void listenToProductsList() {
        viewModel.getProductsList().observe(this, productModels -> Log.i(MainActivity.this.getLocalClassName().toString(),"products size: "+productModels.size()));
    }
}
