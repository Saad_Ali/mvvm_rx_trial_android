package com.example.mvvm_rx_trial_android.Products.model.remote;

import android.arch.lifecycle.LiveData;

import com.example.mvvm_rx_trial_android.helpers.livedata.ApiResponse;
import com.example.mvvm_rx_trial_android.Products.model.ResponseModel.ProductsResponseModel;

import retrofit2.http.GET;

public interface ProductService {

    @GET("customer/getProducts/")
    LiveData<ApiResponse<ProductsResponseModel>> getProductList();
}
