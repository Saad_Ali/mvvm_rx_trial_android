package com.example.mvvm_rx_trial_android.Products.model.repo;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.mvvm_rx_trial_android.Products.model.BasicModels.ProductModel;
import com.example.mvvm_rx_trial_android.Products.model.ResponseModel.ProductsResponseModel;
import com.example.mvvm_rx_trial_android.Products.model.local.ProductDao;
import com.example.mvvm_rx_trial_android.Products.model.remote.ProductService;
import com.example.mvvm_rx_trial_android.helpers.AppExecutors;
import com.example.mvvm_rx_trial_android.helpers.ShouldFetch;
import com.example.mvvm_rx_trial_android.helpers.livedata.ApiResponse;
import com.example.mvvm_rx_trial_android.helpers.livedata.NetworkBoundResource;
import com.example.mvvm_rx_trial_android.helpers.livedata.Resource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProductsRepository {

    private final ProductService webService;
    private AppExecutors appExecutors;
    private ProductDao dao;

    @Inject
    public ProductsRepository(ProductService productService, ProductDao dao) {
        appExecutors = new AppExecutors();
        webService = productService;
        this.dao = dao;
    }

    public LiveData<Resource<List<ProductModel>>> getProductsList() {
        return new NetworkBoundResource<List<ProductModel>, ProductsResponseModel>(appExecutors) {
            @Override
            protected void onFetchFailed() {
                loadFromDb();
            }

            @Override
            protected void saveCallResult(@NonNull ProductsResponseModel item) {
                dao.insertProductList(item.getData().getProductsList());
            }

            @Override
            protected boolean shouldFetch(@Nullable List<ProductModel> data) {
//                return ShouldFetch.networkRecommended(); //TODO check if cached
                return data==null || data.size()==0;
            }

            @NonNull
            @Override
            protected LiveData<List<ProductModel>> loadFromDb() {
                return dao.getProductList();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<ProductsResponseModel>> createCall() {
                return webService.getProductList();
            }
        }.asLiveData();
    }

}
