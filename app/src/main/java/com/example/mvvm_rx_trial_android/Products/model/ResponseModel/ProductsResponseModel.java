package com.example.mvvm_rx_trial_android.Products.model.ResponseModel;

import com.example.mvvm_rx_trial_android.Products.model.BasicModels.ProductsListModel;
import com.google.gson.annotations.SerializedName;

public class ProductsResponseModel {

    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ProductsListModel data;
    @SerializedName("id")
    private String id;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductsListModel getData() {
        return data;
    }

    public void setData(ProductsListModel data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
