package com.example.mvvm_rx_trial_android;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.example.mvvm_rx_trial_android.di.AppComponent;
import com.example.mvvm_rx_trial_android.di.DaggerAppComponent;
import com.example.mvvm_rx_trial_android.di.module.ApiModule;
import com.example.mvvm_rx_trial_android.di.module.AppModule;
import com.example.mvvm_rx_trial_android.di.module.DaoModule;
import com.example.mvvm_rx_trial_android.di.module.NetModule;

public class App extends MultiDexApplication {
    private static AppComponent appComponent;
    private static App instance;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static Context get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent = DaggerAppComponent.builder().
                appModule(new AppModule(this))
                .apiModule(new ApiModule())
                .netModule(new NetModule("")) // put your host url here
                .daoModule(new DaoModule())
                .build();
    }
}
