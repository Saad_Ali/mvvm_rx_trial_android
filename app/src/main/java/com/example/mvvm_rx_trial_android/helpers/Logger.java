package com.example.mvvm_rx_trial_android.helpers;

import android.util.Log;
import com.example.mvvm_rx_trial_android.BuildConfig;


public class Logger {

    private static final String TAG = "Trial_MVVM";

    /**
     * @param str
     */
    public static void v(String str) {
        if (BuildConfig.DEBUG)
            Log.v(TAG, str);
    }

    /**
     * @param str
     */
    public static void i(String str) {
        if (BuildConfig.DEBUG)
            Log.i(TAG, str);
    }

    /**
     * @param str
     * @param e
     */
    public static void e(String str, Throwable e) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, str, e);
    }

    /**
     * @param str
     */
    public static void d(String str) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, str);
    }

}

