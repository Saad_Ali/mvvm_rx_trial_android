package com.example.mvvm_rx_trial_android.helpers;


import com.example.mvvm_rx_trial_android.App;

public class ShouldFetch {
    public static boolean networkRecommended() {
        return NetworkUtils.isNetworkAvailable(App.get());
    }
}
