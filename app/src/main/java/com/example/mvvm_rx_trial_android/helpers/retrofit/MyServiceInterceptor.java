package com.example.mvvm_rx_trial_android.helpers.retrofit;

import android.support.annotation.NonNull;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Singleton
public class MyServiceInterceptor implements Interceptor {
    private Request.Builder requestBuilder;

    @Inject
    MyServiceInterceptor() {
    }



    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();

        requestBuilder = request.newBuilder();

        addAuthenticationHeader();
        return chain.proceed(requestBuilder.build());
    }

    private void addAuthenticationHeader() {

            requestBuilder.addHeader("authorization", ""); // put your token here or remove it if your API have authorization token

    }


}